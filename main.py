import random
from typing import Annotated
import redis 
from fastapi import Depends, FastAPI
from pydantic import BaseModel

app = FastAPI()
redis_client = redis.Redis(host='redis', port=6379, db=0)

class Number(BaseModel):
    value: int

@app.post("/")
def get_index(
    num: Annotated[Number, Depends()]
    ):
    if redis_client.exists('array'):
        array = eval(redis_client.get('array'))
    else:
        array = generate_array()
        redis_client.set('array', str(array))

    return binary_search(array, num.value)

def generate_array():
    arr = []
    for i in range(100):
        arr.append(random.randint(1, 100))
        arr.sort()
    return arr

def binary_search(data, elem):
    low = 0
    high = len(data) - 1

    while low <= high:
        middle = (low + high)//2
        if data[middle] == elem:
            return {"index": middle}
        elif data[middle] > elem:
            high = middle - 1
        else:
            low = middle + 1

    return {"index": -1}
